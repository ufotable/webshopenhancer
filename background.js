console.log('----- background.js started -----')

const filter = {
  urls: ["*://ufotablewebshop.zendesk.com/agent/*"]
};

browser.tabs.onUpdated.addListener((tabId, data) => {
  console.log(data.status)
  if(data.status === 'complete') {
    const message = {
      type: "fire"
    };
    browser.tabs.sendMessage(tabId, message, null)
  }
}, filter)