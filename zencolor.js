
browser.runtime.onMessage.addListener((req, sender, sendResponse) => {
  console.log(req)
  const prev = document.querySelector('.open-in-colorme');
  if(prev) {
    prev.remove();
  }
  if(req.type === "fire") {
    let target = undefined;
    let doFetch = setInterval(() => {
      target = document.querySelector('.ember-view.workspace:not([style*="display:none"]):not([style*="display: none"])');
      if (target !== null) {
        console.log('target found:')
        console.log(target)
        clearInterval(doFetch);
        const link = `https://admin.shop-pro.jp/?mode=sales_lst&type=SRH&email=${target.querySelector('.sender .email').textContent}`;
        const linkElement = document.createElement('a');
        linkElement.classList.add('open-in-colorme');
        linkElement.href = link;
        linkElement.innerText = '発注情報を見る'
        linkElement.target = '_colorme'
        target.querySelector('.ember-view.btn-group').appendChild(linkElement);
      };
    }, 1000)
  }
})