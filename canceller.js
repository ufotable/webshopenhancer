console.log('canceller.js');
const COLORME_BEARER_TOKEN = '4bbb5ee82064ffd5b9209cc9b8c6466e348677bc2b74a0ff1034c8cb4c52cfef';
let items;
let header;


const execCancel = (order) => {
  const check = confirm(`
  この注文(No.${order})をキャンセルします。よろしいですか?\n
  この動作はやり直すことができません。
  `)
  if (check === true) {
    console.log('cancelling...')
    console.log(`https://api.shop-pro.jp/v1/sales/${order}/cancel.json`);
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${COLORME_BEARER_TOKEN}`
      }
    }
    fetch(
      `https://api.shop-pro.jp/v1/sales/${order}/cancel.json`,
      {...options, body: JSON.stringify({'restock': false})}
    )
      .then(res => res.json())
      .then(res => {
        console.log(res);
        if(res.sale.canceled === true) {
          fetch(
            `https://api.shop-pro.jp/v1/sales/${order}.json`,
            {...options, body: JSON.stringify({
              "sale": {
                "paid": false
              }
            })}
          ).then(res => {
            const tr = document.querySelector(`tr.ordercode_${order}`);
            const td = document.querySelector(`td.order${order}`);
            const cancelImage = document.createElement('img');
            cancelImage.classList.add('gn_va_b');
            cancelImage.src = './common/img/icon_order_cancel.gif';
            cancelImage.alt = '受注キャンセル';
            cancelImage.title = '受注キャンセル';
            tr.querySelector('td:nth-child(6)').innerHTML = '未';
            tr.querySelector('td:first-child').appendChild(cancelImage);
            tr.classList.add('sales-lst__cancelled');
            td.classList.toggle('order_cancelready', 'order_cancelled');
          })
        }
      });
  } else {
    return
  }
}

const searchElm = setInterval(() => {
  items = document.querySelectorAll('tr.c-table__tbody-tr');
  header = document.querySelector('.c-table.c-table--center.u-mar-b-5 thead tr');
  console.log(items);
  console.log(header);
  if(items && header) {
    clearInterval(searchElm);
    const theader = document.createElement('th');
    theader.classList.add(
      'c-table__thead-th', 'c-table__thead-th--narrow', 'c-table__cell--5em',
      'u-text-align-center', 'u-nowrap', 'cellttl_condition', 'sales-lst__cancel'
    )
    theader.innerText = ''
    header.appendChild(theader);
    // header.insertBefore(theader, document.querySelector('th.c-table__thead-th.c-table__thead-th--narrow.c-table__cell--5em.u-text-align-center.u-nowrap.cellttl_salesid'))
    items.forEach(tr => {
      if(tr.classList.contains('sales-lst__cancelled')) {
        const innerElement = document.createElement('button');
        innerElement.setAttribute('disabled', true);
        innerElement.textContent = 'キャンセル済';
        const element = document.createElement('td');
        element.classList.add(
          'c-table__tbody-td', 'c-table__tbody-td--narrow', 'u-text-align-center',
          'sales-lst__tbody-td', 'order_cancelled'
        );
        element.appendChild(innerElement);
        tr.appendChild(element);
        // tr.insertBefore(
        //   element,
        //   tr.querySelector('td.c-table__tbody-td.c-table__tbody-td--narrow.u-text-align-center.sales-lst__tbody-td:first-child')
        // )
      } else {
        const order = tr.querySelector('.sales-lst__sales-id').innerText;
        tr.classList.add(`ordercode_${order}`)
        const innerElement = document.createElement('button');
        innerElement.type = 'button';
        innerElement.textContent = 'キャンセルする';
        innerElement.onclick = (e) => execCancel(order);
        const element = document.createElement('td');
        element.classList.add(
          'c-table__tbody-td', 'c-table__tbody-td--narrow', 'u-text-align-center',
          'sales-lst__tbody-td', 'order_cancelready', `order${order}`
        );
        element.appendChild(innerElement);
        tr.appendChild(element);
        // tr.insertBefore(
        //   element,
        //   tr.querySelector('td.c-table__tbody-td.c-table__tbody-td--narrow.u-text-align-center.sales-lst__tbody-td:first-child')
        // )
      }
    })
  }
}, 1000)
